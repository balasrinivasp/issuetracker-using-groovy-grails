package issuetracker

class CreateIssueController {
	
	def view = {
		render(view:"create", model: [Issue.list()]);
	}
	
    def create = { 
		redirect action: 'success', params: params
	}
	
	def success = {
		render(view:"success");
	}
}
