package issuetracker

class Issue {
	
	int issueId
	String issue
	String issueType
	String issueDescription
	String submittedBy
	Date submittedOn
	String assignedTo
	Date assignedOn
	String issueStatus
	String priority
	String comments
	String projects
	
	
    static constraints = {
		issueType(inList: ["Functional", "Cosmetic", "Business"])
		issueStatus(inList: ["New", "Accepted", "Closed", "Not an Issue"])
		priority(inList: ["low", "medium", "high"])
		issueDescription(size: 0..5000)
		comments(size: 0..5000)
		projects(inList: ["prj1", "prj2", "prj3"])
    }
}
