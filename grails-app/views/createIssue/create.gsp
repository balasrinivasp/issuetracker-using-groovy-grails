<!DOCTYPE html>
<html>
	<head>
		<%@ page import="issuetracker.Issue" %>
		
		<meta name="layout" content="main"/>
		<title>Welcome to IssueTracker</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<h1>USEFUL LINKS</h1><br>
			<ul>
				<li style="cursor: pointer;"><g:link url="/issuetracker/">Home</g:link></li>
				<li style="cursor: pointer;"><g:link controller="createIssue" action="view">Create Issue</g:link></li>
				<li style="cursor: pointer;"><g:link controller="viewIssue" action="view">View All</g:link></li>
			</ul>
		</div>
		<div id="page-body" role="main">
			<h1>Create Issue</h1><br><br>
			
			<g:form controller="createIssue" action="create">
				<table>
					<tr>
        				<td><label for="name">Issue Name:</label></td>
        				<td><g:textField name="issue" maxlength="50" value=""></g:textField></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Issue Type:</label></td>
        				<td><g:select name="issueType" from="${['','Functional', 'Cosmetic', 'Business']}" ></g:select></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Issue Description:</label></td>
        				<td><g:textArea name="issueDescription"></g:textArea> </td>
        			</tr>
        			<tr>	
        				<td><label for="name">Submitted By:</label></td>
        				<td><g:textField name="submittedBy" maxlength="50" value=""></g:textField></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Assigned To:</label></td>
        				<td><g:textField name="assignedTo" maxlength="50" value=""></g:textField></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Status:</label></td>
        				<td><g:select name="issueStatus" from="${['','New', 'Accepted', 'Closed', 'Not an Issue']}" ></g:select></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Priority:</label></td>
        				<td><g:select name="priority" from="${['','low', 'medium', 'high']}" ></g:select></td>
        			</tr>
        			<tr>	
        				<td><label for="name">Comments:</label></td>
        				<td><g:textArea name="comments"></g:textArea> </td>
        			</tr>
        		</table>
    			<input value="Submit" type="submit">	
			</g:form>
		</div>
	</body>
</html>
