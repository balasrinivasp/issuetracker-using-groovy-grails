<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to IssueTracker</title>
		<script type="text/javascript">
			function showDetails(issueId) {
				alert('<<Issue details to be displayed in a jquery div dialog>>');	
			}
		</script>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body style="width: 100%; height: 100%;" >
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<h1>USEFUL LINKS</h1><br>
			<ul>
				<li style="cursor: pointer;"><g:link url="/issuetracker/">Home</g:link></li>
				<li style="cursor: pointer;"><g:link controller="createIssue" action="view">Create Issue</g:link></li>
				<li style="cursor: pointer;"><g:link controller="viewIssue" action="view">View All</g:link></li>				
			</ul>
		</div>
		<div id="page-body" role="main"><br><br><br><br>
			<h1>Issues</h1>
			<table>
			<tr><th>Issue Id</th><th>Issue Name</th><th>Issue Type</th><th>Submitted By</th><th>Submitted Date</th><th>Assigned To</th><th>Status</th></tr>
			<tr><td><a href="javascript: showDetails(101);">101</a></td><td>issue1</td><td>Functional</td><td>bpillari</td><td><g:formatDate format="MM-dd-yyyy" date="${new Date()}" /> </td><td>bpillari</td><td>New</td></tr>
			<tr><td><a href="javascript: showDetails(101);">102</a></td><td>issue2</td><td>Cosmetic</td><td>bpillari</td><td><g:formatDate format="MM-dd-yyyy" date="${new Date()}" /> </td><td>bpillari</td><td>Closed</td></tr>
			<tr><td><a href="javascript: showDetails(101);">103</a></td><td>issue3</td><td>Functional</td><td>bpillari</td><td><g:formatDate format="MM-dd-yyyy" date="${new Date()}" /> </td><td>bpillari</td><td>New</td></tr>
			</table>
		</div>
		<div id="dialog" title="Basic dialog" style="display: none;">
			<!-- details to be populated for the issue id -->
		</div>
	</body>
</html>

